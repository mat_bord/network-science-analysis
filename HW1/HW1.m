close all
clear all
clc

% Import data from the file created with python. This file contains three columns: the first and the second 
%column are used to identify the link and the third column contains the year in which the link was created.
G = importdata('Total100m.txt', '\t');

% Create the Adjacency matrix Y
% Year's range goes from 1896 to 2016.
N = max(max(G.data(:,[1 2])));
Y = sparse(G.data(:,2),G.data(:,1),G.data(:,3),N,N);

links = G.data(:,1:2); %get the first and the second element from every line
clear G;

% Select range of years (1896 - 2016)
year_lower = 1896;
year_upper = 2016;
pos = find(Y >= year_lower & Y <= year_upper);
A = zeros(N,N);
A(pos) = 1; % set to 1 all that Iare interested in
%% %%%%%%%%%%%%%%%%%%%%%%%%%%% GRAPH PROPERTIES%%%%%%%%%%%%%%%%%% 
G = graph(A);

% Properties of the graph
L = numedges(G);
dist = distances(G);
dist2=dist(dist~=Inf);
meanDist = sum(sum(dist2))/N^(2);
diameter = max(dist2);

distance=unique(dist(:))';
pdist= histc(dist(:),distance);
figure(4)
plot(distance, pdist, '.');
hold on
grid;
plot(distance, pdist);
hold off

% Print results
disp(['Number of Edges: ' num2str(L)]);
disp(['Number of Nodes: ' num2str(N)]);
disp(['Mean distance: ' num2str(meanDist)]);
disp(['Diameter: ' num2str(diameter)]);

%% %%%%%%%%%%%%%%%%% EXTRACT DEGREE DISTRIBUTION %%%%%%%%%%%%%%%%%%%%%%%%%

% Degree Distribution
d = full(sum(A,2)); %1= out degree, 2 = in degree 
mean_d = mean(d); 
second_mom = mean(d.^2);
third_mom = mean(d.^3);
d_min=min(d);
d_max=max(d);
d = d(d>0); % avoid zero degrees
k = unique(d); % degree samples
pk = histc(d,k)'; % counts occurrences
pk = pk/sum(pk); % normalize to 1

disp(['Average degree: ' num2str(mean_d)])
disp(['Second momentum of degree: ' num2str(second_mom)])
disp(['Third momentum of degree: ' num2str(third_mom)])
%% Cumulative distribution
Pk = cumsum(pk,'reverse');

% log binning
klog = 10.^(0:0.1:ceil(log10(max(k)))); % this identifies the intervals
pklog = histc(d,klog)'; % counts occurrencies
pklog = pklog/sum(pklog); % normalize to 1


%% %%%%%%%%%%%%%%%%% PURE ML FITTING %%%%%%%%%%%%%%%%%%%%%%%%%

kmin=70;
d2 = d(d>=kmin); % restrict range
ga = 1+1/mean(log(d2/kmin)); % estimate the exponent
disp(['Gamma ML = ' num2str(ga)])


c = (ga-1)*kmin^(ga-1); % constant c
C = sum(pk(k>=kmin)); % constant C
                      % correction factor taking into account for  
                      % the fractional impact of the degrees k>=kmin

                      
                      
% fitting in the PDF signal
s1 = C*c*k.^(-ga); 
% fitting in the CCDF signal
s3 = C*c/(ga-1)*k.^(1-ga);
% fitting in the log version of the PDF signal
s2 = C*c/(ga-1)*klog.^(1-ga) *(1-(klog(2)/klog(1))^(1-ga));

                        
%% %%%%%%%%%%%%%%%%% SHOW THE RESULTS %%%%%%%%%%%%%%%%%%%%%%%%%

figure(1)
spy(A);

figure(2)
subplot(2,2,1)
plot(k,pk,'.')
grid
xlabel('k')
ylabel('PDF')
title('linear PDF plot')
subplot(2,2,2)
loglog(k,pk,'.')
hold on
loglog(k,s1);
hold off
axis([xlim min(pk/2) 2*max(pk)])
grid
xlabel('k')
ylabel('PDF')
title('logarithmic PDF plot')
subplot(2,2,3)
loglog(klog,pklog,'.')
hold on
loglog(klog,s2);
hold off
axis([xlim min(pklog/2) 2*max(pklog)])
grid
xlabel('k')
ylabel('PDF')
title('logarithmic PDF plot (log bins)')
subplot(2,2,4)
loglog(k,Pk,'.')
hold on
loglog(k,s3);
hold off
axis([xlim min(Pk/2) 2])
grid
xlabel('k')
ylabel('CCDF')
title('logarithmic CCDF plot')



%% %%%%%%%%%%%%%%%%%%%% CLUSTERING COEFFICIENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C =diag(A*triu(A)*A)*2./(d.*(d-1));
% To avoid nan (which appear when the degree of the node is one)
C(isnan(C)) = 0;


%% %%%%%%%%%%%%%%%%%%%% SHOW THE RESULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(3)
grid
mean_c = mean(C);
disp(['Clustering Coeff.= ' num2str(mean_c)]);
plot(d,C,'.')
hold on
plot(d,d*0+mean_c)
hold off
xlabel('k')
ylabel('C(k)')
legend('Clustering Coefficient','Average Clustering Coefficient')




%% %%%%%%%%%%%%%%%%%%%%% ML FITTING WITH SATURATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%

kmin = min(d);

for ks = 1:max(k)
    tmp = mean(log((d+ks)/(kmin+ks)));
    ga3(ks) = 1+1/tmp;
    de(ks) = log(ga3(ks)-1)-log(kmin+ks)-ga3(ks)*tmp;
end
[~,ks] = max(de);
disp(['k_sat  ML sat = ' num2str(ks)])
disp(['Gamma ML sat = ' num2str(ga3(ks))])


%% %%%%%%%%%%%%%%%%%%%%%% SHOW THE RESULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(5)

% Data
loglog(k,Pk,'.')
hold on

% ML fitting
loglog(k,s3);

% ML fitting with saturation
s1 = ((k+ks)/(kmin+ks)).^(1-ga3(ks));
loglog(k,s1)
hold off
axis([xlim min(Pk/2) 2])
grid
xlabel('k')
ylabel('CCDF')
title('ML fittings')
legend('data','ML 1','ML with sat.')



%% %%%%%%%%%%%%%%%%% EVALUATE ASSORTATIVITY %%%%%%%%%%%%%%%%%%%%%%%%%
% averages of neighbours
k_tmp = (A*d)./d;

% extract averages for each value of k
u = unique(d);
for k = 1:length(u)
    k_nn(k) = mean(k_tmp(d==u(k))); 
end

% do the linear fitting
p = polyfit(log(u'),log(k_nn),1);
disp(['Assortativity factor ' num2str(p(1))])

%% %%%%%%%%%%%%%%%%% SHOW THE RESULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(6)
loglog(d,k_tmp,'g.');
hold on
loglog(u,exp(p(2)+log(u)*p(1)),'r-');
loglog(u,k_nn,'k.');
hold off
grid
xlabel('k')
ylabel('knn(k)')
legend('Nodes neighbours degree','Polynomial fit','Avarage neighbours degree')
title('Assortativity')

%% %%%%%%%%%%%%%%%%%%%%% CUTOFF %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% NATURAL CUTOFF 
nk= kmin*N^(1/(ga-1));

% STRUCTURAL CUTOFF
ks=(mean_d*N)^0.5;  

disp(['Natural cutoff = ' num2str(nk)]);% NATURAL CUTOFF
disp(['kmin = ' num2str(kmin)]);
disp(['Structural cutoff = ' num2str(ks)]);% STRUCTURAL CUTOFF



%% %%%%%%%%%%%%%%%%% ROBUSTNESS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
inhomRatio = mean(d.^2)/mean_d; % here inhomogeneity ratio is > 2
disp(['inhomogeneity ratio= ' num2str(inhomRatio)]);
%breaking points
if ga>2 && ga<3 
    fc=1- 1/[((ga-2)/(3-ga))*(kmin^(ga-2))*(nk.^(3-ga))-1];
elseif ga>3 %ga=gamma
    fc=1-1/[(ga-2)/(ga-3)*kmin-1];
end
 disp(['breaking point of the network= ' num2str(fc)]);
 
%robustness to random failures
fail_rate = 0.03;
H=G;
failAnalysis=zeros(100,1);
elementsToBeRemoved=round(N*fail_rate);
for i = 1:100
    bins = conncomp(H);  %components
    Ngc=max( histc(bins, unique(bins))); %dimension of the GC
    failAnalysis(i) = Ngc/N; %ratio of the Ngc over the original N
    
    %selection of node to remove at random
    for j=1: elementsToBeRemoved
        if(numedges(H)>0)
            H=rmnode(H,randi(max(max(H.Edges.EndNodes))));
        end
    end
end
clear H;

%robustness to attacks
H=G;
attackAnalysis=zeros(N,1);
for i = 1:N
    
    bins = conncomp(H); %components
    Ngc=max( histc(bins, unique(bins)));%dimension of the GC
    attackAnalysis(i) = Ngc/N; %ratio of the Ngc over the original N

    % find and remove the highest degree node
    maxDegree=max(degree(H));
    indexVect= find(degree(H)==maxDegree, 1);
    H=rmnode(H,indexVect);
end
clear H;

%robustness
figure(7)
plot(linspace(0,1),failAnalysis,'o-')
hold on 
plot(linspace(0,1,N),attackAnalysis,'-r')
hold off
xlabel('f')
ylabel('$P_{inf}(f)/P_{inf}$(0)')
title('Robustness plot')
legend('Random failures','Attack')


%% %%%%%%%%%%%%%%%%% TEMPORAL EVOLUTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Set all zeros to 2500 that is a number higher than the max year present in the net
%Y(Y==0) = 2500;
%save('TotMatrixY.mat', 'Y');

%I already saved the  matrix into "TotMatrixY.mat" because it takes a lot of minutes to set all zeros to 2500
S = load('TotMatrixY.mat');
G = graph(A);
y_Add_Un = full(min(S.Y))';
y_Add_Or = sort(unique(y_Add_Un));
numberNode = size(y_Add_Or,1);
m = zeros(numberNode,1);

% Growth of nodes with regard to years
Nyear = histc(y_Add_Un,y_Add_Or)'; % counts occurrences
Nev = cumsum(Nyear);

% Growth of nodes with regard to years
% I start from the final graph and I analyze what happens when I remove a node
for i = 1:1:size(y_Add_Or,1)
    nodes = find(y_Add_Un == y_Add_Or(i)); 
    y_Add_Un(nodes) = []; % Remove the reference of the node from the year vector
    % Start from the bigger
    % Remove all the nodes add in the specific year
    for j = size(nodes,1):-1:1 
        % Remove node and save the number of link of the new graph
        G = rmnode(G,nodes(j)); 
    end
    m(numberNode) = numedges(G); 
    numberNode = numberNode-1;
end

%% %%%%%%%%%%%%%%%%% SHOW THE RESULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(8)
subplot(1,2,1)
title('Network Evolution')
hold on
xlabel('Years')
ylabel('Number of Nodes')
title('Growth of the network')
plot(y_Add_Or,Nev);
hold off

figure(8)
subplot(1,2,2)
hold on
xlabel('Years')
ylabel('Number of Edges')
title('Growth of the network')
plot(y_Add_Or,m);
hold off

%% %%%%%%%%%%%%%%%%% PREFERENTIAL ATTACHMENT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evolution of the Net
%The same solution applied in the temporal evolution
S = load('TotMatrixY.mat');
Y=S.Y;
T = size(y_Add_Or,1);
K_Evol = zeros(N,T);

% Evolution in time of the degree
for y = 1:1:T
    A = zeros(N,N);
    A(Y < y_Add_Or(y)) = 1;
    K_Evol(:,y)=sum(A)';
end

T = length(y_Add_Or); % Max instant

Pref = zeros(length(d),T);
% Compute the Preferential attachment between each year
for i=1:length(K_Evol)
    for j=1:T-1
        Pref(i,j) = (K_Evol(i,j+1)-K_Evol(i,j))/(Nev(j+1)-Nev(j));
    end
end

% Compute the average of Preferential Attachment
MaxK = max(K_Evol(:));
PrefMean = zeros(MaxK,1);
for i=1:1:MaxK
    pos = find(K_Evol(:,2:T) == i);
    PrefT = Pref(pos);
    PrefMean(i) = mean(PrefT);
end
PrefMean(isnan(PrefMean))=0;
% Compute Cumulative sum of Preferential Attachment
CumPref = cumsum(PrefMean);
CumPref2=CumPref(CumPref>0);
%% %%%%%%%%%%%%%%%%% SHOW THE RESULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Limit Lines
kx1 = (1:MaxK).^1;
kx1 = kx1/kx1(1)*CumPref2(1);
kx2 = (1:MaxK).^2;
kx2 = kx2/kx2(1)*CumPref2(1);

figure(9)
loglog(1:(MaxK-9),CumPref2,'.');
grid on
hold on
loglog(1:MaxK,kx1,'Color',[102/255 0/255 255/255]);
loglog(1:MaxK,kx2,'Color',[204/255 102/255 255/255]);
xlabel('k');
ylabel('\pi(k)');
legend('Network','k','k^2');
