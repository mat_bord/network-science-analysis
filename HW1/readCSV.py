import pandas as pd
import time
file = pd.read_csv('./athlete_events.csv')
a=file[(file.Sport == 'Athletics') & (file.Season == 'Summer') & (file.Event == 'Athletics Men\'s 100 metres') ]
b=a[['ID','Year']]
b=b.drop_duplicates(subset=['ID', 'Year'])
i=0
current='0'
for k in range(len(b['ID'])):
    if current==b.iloc[k].ID:
        b.iloc[k].ID=i
    else:
        current=b.iloc[k].ID
        i=i+1
        b.iloc[k].ID=i

#b=b.sort_values(by=['Year'])
#print(len(b['ID']))
        
"""start_time=	time.time()
df=pd.DataFrame({"Node1":[],"Node2":[],"Year":[]})
for i in range(len(b['ID'])):
	start_time = time.time()
	for k in range(len(b['ID'])):
	    if k != i and b.iloc[i].Year==b.iloc[k].Year:
	    	r = {"Node1":[(b.iloc[i].ID)],"Node2":[(b.iloc[k].ID)],"Year":[(b.iloc[k].Year)]}
	    	df2=pd.DataFrame(r)
	    	df = df.append(df2)

	with pd.option_context('display.max_rows', None, 'display.max_columns', None):
		with open('out'+str(i)+'.txt', 'w') as f:
			print(df.to_string(index=False), file=f)
			print("--- %s seconds ---" % (time.time() - start_time))
"""
b=b.sort_values(by=['ID'])
ren = range(len(b['ID']))
df = pd.DataFrame([ {"Node1":b.iloc[i].ID,"Node2":b.iloc[k].ID,"Year":b.iloc[k].Year} for i in ren for k in ren if k != i and b.iloc[i].Year==b.iloc[k].Year])
with pd.option_context('display.max_rows', None, 'display.max_columns', None):
	with open('out.txt', 'w') as f:
		print(df.to_string(index=False), file=f)


#100metres: 1900 nodes
#all specialities: 30536 nodes