close all
clear all
clc

% Import data from the file created with python. This file contains three columns: the first and the second 
%column are used to identify the link and the third column contains the year in which the link was created.
G = importdata('Total100m.txt', '\t');

%%%%%%%%   NAMES  %%%%%%%%%%%%%%
% Import the name associated to each node.
fid = fopen('IDName.txt');
% Read all lines & collect in cell array
txt = textscan(fid,'%s','delimiter',''); 
node_names = txt{1,1}(2:1:end);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%   YEAR  %%%%%%%%%%%%%%
% Import the name associated to each node.
fid = fopen('Year.txt');
% Read all lines & collect in cell array
txt = textscan(fid,'%s','delimiter',''); 
node_years = txt{1,1}(2:1:end);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create the Adjacency matrix Y
% Year's range goes from 1896 to 2016.
N = max(max(G.data(:,[1 2])));
A = sparse(G.data(:,2),G.data(:,1),G.data(:,3),N,N);
links = G.data(:,1:2); %get the first and the second element from every line
%clear G;

% Select range of years (1896 - 2016)
year_lower = 1896;
year_upper = 2016;
pos = find(A >= year_lower & A <= year_upper);
A = zeros(N,N);
A(pos) = 1; % set to 1 all that I am interested in

Au = 1*(A+A'>0); % undirected network


% remove nodes which are NOT connected
pos = find(sum(A)~=0);
A = A(pos,pos);

% remove dead ends (until none avalable)
exit = false;
while (~exit)
    pos = find(sum(A)~=0);
    A = A(pos,pos);
    N = size(A,1);
    exit = isempty(find(sum(A)==0, 1));
end

% find the largest connected component
e1 = [1;zeros(N-1,1)];
exit = false;
while(~exit)
    e1_old = e1;
    e1 = 1*(A*e1+e1>0);
    exit = (sum(e1-e1_old)==0);
end
pos = find(e1);
A = A(pos,pos);
idx=find(e1==0); %cells equal to zeros don't belong to the giant component
node_names(idx,:)=[]; %in every index I found I put []
node_years(idx,:)=[];

N = size(A);
N = N(1);
%% %%%%%%%%%%%%%%%%% RANKING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PageRank

M = A*sparse(diag(1./sum(A)));
c = 0.85;
q = ones(N,1)/N;
disp('Computing PageRank - linear system solution')
tic
r = sparse((eye(N)-c*M)/(1-c))\q;
r = r/sum(r);
toc

disp('Computing PageRank - power iteration')
tic
p0 = ones(N,1)/N;
s = [];
for k = 1:35
    p00 = p0;
    p0 = c*M*p0+(1-c)*q;
    p0 = p0/sum(p0);
    s(k) = norm(p0-r)/sqrt(N);
end
toc

% First top 5 node according to
% PageRank
[sorted_rank, rank_pos] = sort(r,'descend');
disp('Top 5 athletes according to PageRank:')
disp(node_names(rank_pos(1:5)))

disp('Extracting PageRank eigenvalues')
tic
eig = eigs(M,size(M,1));
toc

figure(1)
set(0,'defaultTextInterpreter','latex') % to use LaTeX format
ref = (c*abs(eig(2))).^(1:35);
semilogy([s;ref/ref(end)*s(end)]')
grid
legend('power iteration','second eigenvalue')
xlabel('k [iteration \#]')
ylabel('$\|r_k - r_\infty\|$')
title('PageRank convergence')

figure(2)
plot(eig,'x')
hold on
plot(exp(2i*pi*(0:0.001:1)))
hold off
grid
title('PageRank eigenvalues')


%%-------- SIMRANK----
%Teleportation vector
qsm=zeros(N,1);
qsm(3)=1;
%sparse linear systemm algorithm
rsm=sparse((eye(N)-c*M)/(1-c))\qsm;
rsm=rsm/sum(rsm);
%power iteration method
p0sm=ones(N,1)/N;
sm=[];
for k=1:35
    p0sm=c*M*p0sm+(1-c)*qsm;
    p0sm=p0sm/sum(p0sm);
    sm(k)=norm(p0sm-rsm)/sqrt(N);
end

figure(3)
set(0,'defaultTextInterpreter','latex') % to use LaTeX format
ref = (c*abs(eig(2))).^(1:35);
semilogy([sm;ref/ref(end)*sm(end)]')
grid
legend('power iteration','second eigenvalue')
xlabel('k [iteration \#]')
ylabel('$\|r_k - r_\infty\|$')
title('SimRank convergence')

%% HITS - authorities

M = A*A';
disp('Computing HITS - eigenvalue extraction')
tic
[pp,ee] = eigs(M,2);
toc
p = -pp(:,1)/norm(pp(:,1));

disp('Computing HITS - power iteration')
tic
N = size(M,1);
p0 = ones(N,1)/sqrt(N);
s = [];
for k = 1:35
    p00 = p0;
    p0 = A*(A'*p0);
    p0 = p0/norm(p0);
    s(k) = norm(p0-p00)/sqrt(N);
end
toc 

% First top 5 node according to HITS
[sorted_rank, rank_pos] = sort(p0,'descend');
disp('Top 5 athletes according to HITS:')
disp(node_names(rank_pos(1:5)))

figure(4)
ref = (ee(2,2)/ee(1,1)).^(1:35);
semilogy([s;ref*s(end)/ref(end)]')
grid
legend('power iteration','second eigenvalue')
xlabel('k [iteration \#]')
ylabel('$\|r_k - r_\infty\|$')
title('HITS convergence - authorities')


%% comparison 

figure(5)
plot([p/sum(p),-r/sum(r)])
grid
legend('HITS','PageRank')
title('PageRank vs HITS ')

figure(5)
plot(p/sum(p),r/sum(r),'x')
grid
xlabel('HITS score')
ylabel('Pagerank score')
title('PageRank vs HITS ')


%% %%%%%%%%%%%%%%%%% COMMUNITY DETECTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% extract eigensystem info

% Laplacian
d = full(sum(A));
Di = spdiags(1./sqrt(d'),0,N,N);
L = speye(N) - Di*A*Di;

% extract and plot eigenvalues
la = eigs(L,N); % eigenvalues
figure
plot(la,'x')
grid
title('eigenvalues (of the normalized Laplacian)')

% extract eigenvectors
[V,DD] = eigs(L,4,'SA');
Vv = Di*V; % normalize eigenvectors
v1 = Vv(:,2)/norm(Vv(:,2)); % Fiedler's vector
v2 = Vv(:,3)/norm(Vv(:,3)); 

%% sweep wrt the ordering identified by v1

% reorder the adjacency matrix
[v1s,pos] = sort(v1);
A1 = A(pos,pos);

% evaluate the conductance measure
a = full(sum(triu(A1)));
b = full(sum(tril(A1)));
d = a+b;
D = sum(d);
assoc = cumsum(d);
assoc = min(assoc,D-assoc);
cut = cumsum(b-a);
conduct = cut./assoc;
conduct = conduct(1:end-1);

% show the conductance measure
figure
plot(conduct,'x-')
grid
title('conductance sweep')

% identify the minimum -> threshold
[~,mpos] = min(conduct(1:end));
threshold = mean(v1s(mpos:mpos+1));
C1 = sort(pos(1:mpos));
C2 = sort(pos(mpos+1:end));

% some data
disp(['Minimum conductance: ' num2str(conduct(mpos))])
disp(['   Cheeger''s lower bound: ' num2str(.5*DD(2,2))])
disp(['   Cheeger''s upper bound: ' num2str(sqrt(2*DD(2,2)))])
disp(['   # of links: ' num2str(D/2)])
disp(['   Cut value: ' num2str(cut(mpos))])
disp(['   Assoc value: ' num2str(assoc(mpos))])
disp(['   Community size #1: ' num2str(mpos)])
disp(['   Community size #2: ' num2str(N-mpos)])
disp(['This is a very good division into two communities!'])

% show network with partition
figure
plot(v1,v2,'.')
grid
hold on
plot(threshold*[1,1],ylim,'r-')
hold off
title('2D representation by Laplacian eigenvectors')

% show the matrix reorganized in communities
figure
spy(A([C1;C2],[C1;C2]))
title('matrix reorganized in communities')

%%%%%%% Years for communities%%%%%%%%%%
comm1years=node_years(C1);
comm1years=sortrows(comm1years);
comm2years=node_years(C2);
comm2years=sortrows(comm2years);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% %%%%%%%%%%%%%%%%% LINK PREDICTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
G = graph(A, 'upper');
links = numedges(G);
%select 10% of the edges as probe set
probe_size= uint16(0.01*links);
%the remaining edges are the test set
test_size=links-probe_size; 

edges=table2array(G.Edges);
    
test_edges = edges(1:test_size,:);
probe_edges = edges(test_size+1:links-1,:);
    
test_graph= graph(test_edges(:,1),test_edges(:,2));
T = adjacency(test_graph);
% remove nodes which are NOT connected
pos = find(sum(T)~=0);
T = A(pos,pos);
nodes = size(T,1);

%% RANDOM WALK
rand_walk = zeros(nodes, nodes);
S_rand_walk = zeros(nodes, nodes);
T_random_walk = T;
G_random_walk = G;
links_ra = numedges(G_random_walk);
M_pr = eye(nodes);

d= transpose(T_random_walk).*ones(nodes,1);
D=diag(d^-1);
M=T_random_walk.*D;  %normalized adj maArix

% find neighbors for each node 
 for i=1:nodes
     N_i = neighbors(G_random_walk,i); %seA of neighbors of i
 end
  for j=1:nodes
      N_j=neighbors(G_random_walk,j);% seA of neighbors of j
  end      
  
for t=1:4 % four sAep are sufficient 
    M = M_pr*M;
 for i=1:nodes
     for j = 1:nodes
         %p_ij is Ahe probability of sAepping on node j from node i
         p_ij = M(i,j);
         p_ji = p_ij; %equivalent probabilities
         rand_walk(i,j) = p_ij.*(length(N_i)/ links_ra)+p_ji*(length(N_j)/ links_ra);
     end 
 end    
    S_rand_walk = S_rand_walk+rand_walk;
    M_pr = M;
end
%Calculation of AUC
T_random_walk=reshape(T_random_walk,[nodes*nodes,1]);
S_rand_walk=reshape(S_rand_walk,[nodes*nodes,1]);
[X,Y,r,AUC] =perfcurve(T_random_walk,S_rand_walk,1);


disp(['AUC for random walks: ' num2str(AUC)]);

%% COMMON NEIGHBOUR

T_common_neigh = T;
G_common_neigh = G;
scc = zeros(nodes, nodes);
for i=1:nodes
    N_i = neighbors(G_common_neigh,i); %seA of neighbors of i
    for j=1:nodes
        N_j=neighbors(G_common_neigh,j); %set of neighbors of j
    
        intersection = intersect(N_i,N_j); % neighbors in common
        
        for t = 1:length(intersection)
            scc(i,j)= d(intersection(t)); 
        end
    end
end 

%Calculation of AUC
T_common_neigh=reshape(T_common_neigh,[nodes*nodes,1]);
scc=reshape(scc,[nodes*nodes,1]);
[X,Y,r,AUC] =perfcurve(T_common_neigh,scc,1);

disp(['AUC for common neighbour: ' num2str(AUC)]);

%% ADAMIC ADAR

T_adamic_adar = T;
G_adam = G;

saa = zeros(nodes, nodes);
for i=1:nodes
    N_i = neighbors(G_adam,i);
    for j=1:nodes
        N_j=neighbors(G_adam,j);    
        intersection = intersect(N_i,N_j); % neighbors in common      
        for t = 1:length(intersection)
            saa(i,j)=saa(i,j)+(1/log(d(intersection(t))));
        end
    end
end 

%Calculation of AUC
T_adamic_adar=reshape(T_adamic_adar,[nodes*nodes,1]);
saa=reshape(saa,[nodes*nodes,1]);
[X,Y,r,AUC] =perfcurve(T_adamic_adar,saa,1);

disp(['AUC for adamic adar: ' num2str(AUC)]);


%% RESOURCE ALLOCATION

T_ra = T;
G_ra = G;
sra=zeros(nodes, nodes);
for i=1:nodes
    N_i = neighbors(G_ra,i);
    for j=1:nodes
        N_j=neighbors(G_ra,j);    
        intersection= intersect(N_i,N_j);
        for t=1: length(intersection)
        sra(i,j)=sra(i,j)+(1/d(intersection(t)));
        end
    end
end 

%Calculation of AUC
T_ra = reshape(T_ra,[nodes*nodes,1]);
sra=reshape(sra,[nodes*nodes,1]);
[X,Y,r,AUC] =perfcurve(T_ra,sra,1);

disp(['AUC for resource allocation: ' num2str(AUC)]);

%% LOCAL path

T_local = T;

beta_lpath = 0.9; 

SLP= T_local^2+beta_lpath*T_local^3;

%Calculation of AUC
T_local=reshape(T_local,[nodes*nodes,1]);
SLP=reshape(SLP,[nodes*nodes,1]);
[X,Y,r,AUC] =perfcurve(T_local,SLP,1);


disp(['AUC for local path: ' num2str(AUC)]);


%% KATZ

T_katz = T;

beta_katz = 0.9;
I=eye(nodes);
SKATZ=inv(I-beta_katz*T_katz)- I;

%Calculation of AUC
T_katz=reshape(T_katz,[nodes*nodes,1]);
SKATZ=reshape(SKATZ,[nodes*nodes,1]);
[X,Y,r,AUC] =perfcurve(T_katz,SKATZ,1);

disp(['AUC for katz: ' num2str(AUC)]);
